## import skeleton process
import FWCore.ParameterSet.Config as cms

process = cms.Process("DAS")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1000) )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
      'root://xrootd-cms.infn.it//store/data/Run2023C/Muon0/MINIAOD/22Sep2023_v4-v1/30000/007bb727-5443-48fa-833a-9c14ea401e9b.root'
    )
)


process.out = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string('slimMiniAOD_data_MuEle.root'),
    outputCommands = cms.untracked.vstring(['drop *', 'keep *_slimmedMuons__*', 'keep *_slimmedElectrons__*'])
)

process.end = cms.EndPath(process.out)
