---
title: "CMS Data Analysis School Pre-Exercises - Third Set"
teaching: 0
exercises: 240
questions:
- "How do I do an analysis with so much data that I cannot run it
interactively on my computer?"
- "What is CRAB? How do I use it to run an analysis
on the grid?"
- "How do configuration files look like?"
- "How do I extract the luminosity of the dataset I analyzed?"
objectives:
- "Become familiar with the basic Grid tools used in CMS for user
analysis"
- "Learn about grid certificate usage"
- "Know what CRAB is and how to use it for your analysis"
- "Know how to use BRILcalc to extract luminosities"
keypoints:
- "Use and validate your grid certificate."
- "Setting up your CRAB configuration and run jobs over the CMS grid."
- "Publish your CRAB datasets."
- "Calculate the luminosities of the datasets processed via CRAB."
---

# Introduction
 This is the third set of CMSDAS exercises. The purpose of these exercises are for the workshop attendees to become familiar with the basic Grid tools used in CMS for user analysis. Please run and complete each of these exercises. However, unlike the previous sets of exercises, **this set will take considerably longer**. Having your storage space set up may take several days, Grid jobs run with some latency, and there can be problems. You should **set aside about a week** to complete these five exercises. The actual effort required is not the whole week but a few hours (more than the previous two sets). If, at any time problems are encountered with the exercise please e-mail [cmsdas-cern-organizers@cern.ch](mailto:cmsdas-cern-organizers@cern.ch) with a detailed description of your problem. For CRAB questions unrelated to passing these exercises, to send feedback and ask for support in case of CRAB related problems, please consult the [CRAB troubleshooting twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3Troubleshoot). All CRAB users should subscribe to the very useful [computing tools CMS talk forum](https://cms-talk.web.cern.ch/c/offcomp/comptools/87).

> ## Note
> **This section assumes that you have access to lxplus at CERN.** Learn more about lxplus
> [here](https://information-technology.web.cern.ch/services/lxplus-service)
> and the [lxplus knowledge guide](https://lxplusdoc.web.cern.ch/).
>
> Later on, you can check with your university contact for Tier 2 or Tier 3 storage area. Once you are granted the write permission to the specified site, for later analysis you can use CRAB as the below exercise but store the output to your Tier 2 or Tier 3 storage area.
>
> AGAIN: To perform this set of exercises, lxplus access, Grid Certificate, and CMS VO membership are required. You should already have these things, but if not, follow these instructions from the [first set of exercises]({{ page.root }}{% link _episodes/01-CMSDataAnalysisSchoolPreExerciseFirstSet.md %}).
>
{: .callout}

> ## Question
> Questions for each exercise are in boxes such as this. <br>
For CMSDAS@CERN {{ site.year }} please submit your answers for the [CMSDAS@CERN Google Form third set][Set3_form].
{: .challenge}


> ## Support
> There is a dedicated Mattermost team, called CMSDAS@CERN24,
> setup to facilitate communication and discussions via live chat
> (which is also archived). You will need your CERN login credentials
> (SSO) and you will need to join the private CMSDAS@CERN24
> team in order to be able to see (or find using the search
> channels functionality) the channels setup for communications
> related to the school. The sign-up link is
> [here](https://mattermost.web.cern.ch/signup_user_complete/?id=kh8yucmk77n3zgha6ncdq3f7br&md=link&sbr=su)
> and the Pre-exercises channel can be found [here](https://mattermost.web.cern.ch/cmsdas24/channels/pre-exercises).
>
{: .testimonial}
<!-- {: .support} -->

# Exercise 10 - Verify your grid certificate is OK

This exercise depends on obtaining a grid certificate and VOMS membership, but does not depend on any previous exercises.
After you've installed your grid certificate, you need to verify it has all the information needed.

Login to **lxplus.cern.ch** and initialize your proxy:
```shell
voms-proxy-init -voms cms
```
{: .source}

Then run the following command:
```shell
voms-proxy-info -all | grep -Ei "role|subject"
```
{: .source}

The response should look like this:
```
subject   : /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=<username>/CN=<number>/CN=<name> <lastname>/CN=<number>
subject   : /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=<username>/CN=<number>/CN=<name> <lastname>
attribute : /cms/Role=NULL/Capability=NULL
```
{: .output}
If you do not have the first attribute line listed above, you have not completed the VO registration above and you must complete it before continuing.

> ## Question 10
> Copy the output corresponding to the text in the output box above. <br>
> For CMSDAS@CERN {{ site.year }} please submit your answers for the [CMSDAS@CERN {{ site.year }} Google Form third set][Set3_form].
{: .challenge}



# Exercise 11 - Obtain a /store/user area and setup CRAB

## Obtain a /store/user area

This exercise depends on **successfully completing Exercise 10**. Completion of this exercise requires a users to have `/store/user/YourCERNUserName` in Tier2 or Tier3 site. (ex, `eos` area at lxplus). and a user should get this automatically once they have a lxplus account.

## CRAB Introduction

In this exercise, you will learn an important tool [CRAB](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab), which is used
in all the data analysis at CMS. CRAB (CMS Remote Analysis Builder) is a utility to submit CMSSW jobs to distributed computing resources. By using CRAB you will be able to access CMS data and Monte-Carlo which are distributed to CMS aligned centres worldwide and exploit the CPU and storage resources at CMS aligned centres. You will also test your grid certificate and your cms EOS storage element which will be useful during CMSDAS@CERN{{ site.year }}.

*Help* or *questions* about CRAB: Follow the [FAQ](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab#Getting_support) to get help with CRAB.

The most recent CRAB3 tutorial is always in the [WorkBook](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBook) under [WorkBookCRABTutorial](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookCRAB3Tutorial). This tutorial provides complete instructions for beginner and expert user to use CRAB in their studies. We strongly recommend you to learn the CRAB tutorial after you finish these exercises. In this exercise, you will use CRAB to generate a MC sample yourself and publish it to the DAS.

## Setup CRAB

In this exercise, we will use `CMSSW_13_0_17`.

You can follow the same instructions from [Exercise 3]({{ page.root }}{% link _episodes/01-CMSDataAnalysisSchoolPreExerciseFirstSet.md %}#exercise-3---setup-a-cmssw-release-area). The instructions are reproduced here:

```shell
cd ~/YOURWORKINGAREA

export SCRAM_ARCH=el9_amd64_gcc11
### If you are using the default tcsh shell (or csh shell)
setenv SCRAM_ARCH el9_amd64_gcc11
###

cmsrel CMSSW_13_0_17
cd CMSSW_13_0_17/src
cmsenv
```
{: .source}


After setting up the CMSSW environment via `cmsenv`, you'll have access to the latest version of CRAB. It is possible to use CRAB from any directory after setup. One can check that the crab command is indeed available and the version being used by executing:

```shell
which crab
```
{: .source}

```
/cvmfs/cms.cern.ch/common/crab
```
{: .output}

or

```shell
crab --version
```
{: .source}

```
CRAB client v3.240416
```
{: .output}

The `/store/user` area is commonly used for output storage from CRAB. When you complete *Exercise 11*, you can follow these instructions to make sure you can read from and write to your space using CRAB command.

Initialize your proxy:

```shell
voms-proxy-init -voms cms
```
{: .source}

Check if you can write to the `/store/user/` area. The crab checkwrite command can be used by a user to check if he/she has write permission in a given CERN eos directory path (by default `/store/user/<HN-username>/`) in a given site. The syntax to be used is:

```shell
crab checkwrite --site= <site-name>
```
{: .source}

For example:

```shell
crab checkwrite --site=T3_CH_CERNBOX
```
{: .source}

 The output should look like this:

> ## Show/Hide
> > ```
> > Rucio client intialized for account <username>
> > Will check write permission in the default location /store/user/<username>
> > Validating LFN /store/user/<username>...
> > LFN /store/user/<username> is valid.
> > Will use `gfal-copy`, `gfal-rm` commands for checking write permissions
> > Will check write permission in /store/user/<username> on site T3_CH_CERNBOX
> > Will use PFN: davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/<hash>.tmp
> > 
> > Attempting to create (dummy) directory crab3checkwrite_<date>_<time>_<hash> and copy (dummy) file <hash>.tmp to /store/user/<username>
> > 
> > Executing command: which scram >/dev/null 2>&1 && eval `scram unsetenv -sh`; gfal-copy -p -v -t 180 file:///afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/<hash>.tmp 'davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/<hash>.tmp'
> > Please wait...
> > 
> > Successfully created directory crab3checkwrite_<date>_<time>_<hash> and copied file <hash>.tmp to /store/user/<username>
> > 
> > Attempting to delete file davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/<hash>.tmp
> > 
> > Executing command: which scram >/dev/null 2>&1 && eval `scram unsetenv -sh`; gfal-rm -v -t 180 'davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/<hash>.tmp'
> > Please wait...
> > 
> > Successfully deleted file davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/<hash>.tmp
> > 
> > Attempting to delete directory davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/
> > 
> > Executing command: which scram >/dev/null 2>&1 && eval `scram unsetenv -sh`; gfal-rm -r -v -t 180 'davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/'
> > Please wait...
> > 
> > Successfully deleted directory davs://eosuserhttp.cern.ch:443//eos/user/<initial>/<username>/crab3checkwrite_<date>_<time>_<hash>/
> > 
> > Checkwrite Result:
> > Success: Able to write in /store/user/<username> on site T3_CH_CERNBOX
> > ```
> {: .output}
{: .solution}

 Choosing the `T3_CH_CERNBOX` "site" allows you to have the option of outputing crab jobs to your *EOS area*, providing you with an easy way to access produced files. However this does not allow for publishing of produced samples as CERNBOX is NOT a CMS storage, and files in there can not be listed in DBS. For more details about crab output options, visit the following [link](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3FAQ).

> ## Question 11
> What is the name of your directory name in eos? <br>
> For CMSDAS@CERN{{ site.year }} please submit your answers for the [CMSDAS@CERN{{ site.year }} Google Form third set][Set3_form].
{: .challenge}


# Exercise 12 - Generate (and publish) a minimum bias dataset with CRAB

## CMSSW configuration file to generate MC events

 In this section we provide an example of a CMSSW parameter-set configuration file to generate minimum bias events with the Pythia MC generator. We call it `CMSDAS_MC_generation.py`. Using CRAB to generate MC events requires some special settings in the CRAB configuration file, as we will show later.

We use the cmsDriver tool to generate our configuration file:

```shell
cmsDriver.py MinBias_14TeV_pythia8_TuneCP5_cfi --mc  --conditions auto:phase1_2023_realistic -n 10 --era Run3_2023 --eventcontent FEVTDEBUG --relval 100000,300 -s GEN,SIM --datatier GEN-SIM --beamspot Realistic25ns13p6TeVEarly2023Collision --fileout file:step1.root --no_exec --python_filename CMSDAS_MC_generation.py
```
{: .source}

If successful, `cmsDriver` will return the following
```
GEN,SIM,ENDJOB
Step: GEN Spec: 
Loading generator fragment from Configuration.Generator.MinBias_14TeV_pythia8_TuneCP5_cfi
Step: SIM Spec: 
Step: ENDJOB Spec: 
Config file CMSDAS_MC_generation.py created
```
{: .output}

 Feel free to investigate (look at) the newly outputted
 `CMSDAS_MC_generation.py`.

## Generating MC events locally

We want to test this Configuration file locally for a small number of events before we submit to CRAB for massive generation. To test this file, we can run

```shell
cmsRun CMSDAS_MC_generation.py
```
{: .source}

This MC generation code will then produce an EDM output file called `step1.root` with the content of a GEN-SIM data tier for 10 generated events.


> ## Show/Hide
>
> ```
> 
> **************************************************************
>  Geant4 version Name: geant4-10-07-patch-02 [MT]   (11-June-2021)
>   << in Multi-threaded mode >> 
>                        Copyright : Geant4 Collaboration
>                       References : NIM A 506 (2003), 250-303
>                                  : IEEE-TNS 53 (2006), 270-278
>                                  : NIM A 835 (2016), 186-225
>                              WWW : http://geant4.org/
> **************************************************************
> 
> 
> *------------------------------------------------------------------------------------* 
> |                                                                                    | 
> |  *------------------------------------------------------------------------------*  | 
> |  |                                                                              |  | 
> |  |                                                                              |  | 
> |  |   PPP   Y   Y  TTTTT  H   H  III    A      Welcome to the Lund Monte Carlo!  |  | 
> |  |   P  P   Y Y     T    H   H   I    A A     This is PYTHIA version 8.306      |  | 
> |  |   PPP     Y      T    HHHHH   I   AAAAA    Last date of change: 28 Jun 2021  |  | 
> |  |   P       Y      T    H   H   I   A   A                                      |  | 
> |  |   P       Y      T    H   H  III  A   A    Now is 29 Apr 2024 at 12:00:36    |  | 
> |  |                                                                              |  | 
> |  |   Program documentation and an archive of historic versions is found on:     |  | 
> |  |                                                                              |  | 
> |  |                               https://pythia.org/                            |  | 
> |  |                                                                              |  | 
> |  |   PYTHIA is authored by a collaboration consisting of:                       |  | 
> |  |                                                                              |  | 
> |  |   Christian Bierlich, Nishita Desai, Leif Gellersen, Ilkka Helenius, Philip  |  | 
> |  |   Ilten, Leif Lönnblad, Stephen Mrenna, Stefan Prestel, Christian Preuss,    |  | 
> |  |   Torbjörn Sjöstrand, Peter Skands, Marius Utheim and Rob Verheyen.          |  | 
> |  |                                                                              |  | 
> |  |   The complete list of authors, including contact information and            |  | 
> |  |   affiliations, can be found on https://pythia.org/.                         |  | 
> |  |   Problems or bugs should be reported on email at authors@pythia.org.        |  | 
> |  |                                                                              |  | 
> |  |   The main program reference is 'An Introduction to PYTHIA 8.2',             |  | 
> |  |   T. Sjöstrand et al, Comput. Phys. Commun. 191 (2015) 159                   |  | 
> |  |   [arXiv:1410.3012 [hep-ph]]                                                 |  | 
> |  |                                                                              |  | 
> |  |   The main physics reference is the 'PYTHIA 6.4 Physics and Manual',         |  | 
> |  |   T. Sjöstrand, S. Mrenna and P. Skands, JHEP05 (2006) 026 [hep-ph/0603175]  |  | 
> |  |                                                                              |  | 
> |  |   PYTHIA is released under the GNU General Public Licence version 2 or later.|  | 
> |  |   Please respect the MCnet Guidelines for Event Generator Authors and Users. |  | 
> |  |                                                                              |  | 
> |  |   Disclaimer: this program comes without any guarantees.                     |  | 
> |  |   Beware of errors and use common sense when interpreting results.           |  | 
> |  |                                                                              |  | 
> |  |   Copyright (C) 2021 Torbjörn Sjöstrand                                      |  | 
> |  |                                                                              |  | 
> |  |                                                                              |  | 
> |  *------------------------------------------------------------------------------*  | 
> |                                                                                    | 
> *------------------------------------------------------------------------------------* 
>
>
> *------------------------------------------------------------------------------------* 
> |                                                                                    | 
> |  *------------------------------------------------------------------------------*  | 
> |  |                                                                              |  | 
> |  |                                                                              |  | 
> |  |   PPP   Y   Y  TTTTT  H   H  III    A      Welcome to the Lund Monte Carlo!  |  | 
> |  |   P  P   Y Y     T    H   H   I    A A     This is PYTHIA version 8.306      |  | 
> |  |   PPP     Y      T    HHHHH   I   AAAAA    Last date of change: 28 Jun 2021  |  | 
> |  |   P       Y      T    H   H   I   A   A                                      |  | 
> |  |   P       Y      T    H   H  III  A   A    Now is 29 Apr 2024 at 12:00:36    |  | 
> |  |                                                                              |  | 
> |  |   Program documentation and an archive of historic versions is found on:     |  | 
> |  |                                                                              |  | 
> |  |                               https://pythia.org/                            |  | 
> |  |                                                                              |  | 
> |  |   PYTHIA is authored by a collaboration consisting of:                       |  | 
> |  |                                                                              |  | 
> |  |   Christian Bierlich, Nishita Desai, Leif Gellersen, Ilkka Helenius, Philip  |  | 
> |  |   Ilten, Leif Lönnblad, Stephen Mrenna, Stefan Prestel, Christian Preuss,    |  | 
> |  |   Torbjörn Sjöstrand, Peter Skands, Marius Utheim and Rob Verheyen.          |  | 
> |  |                                                                              |  | 
> |  |   The complete list of authors, including contact information and            |  | 
> |  |   affiliations, can be found on https://pythia.org/.                         |  | 
> |  |   Problems or bugs should be reported on email at authors@pythia.org.        |  | 
> |  |                                                                              |  | 
> |  |   The main program reference is 'An Introduction to PYTHIA 8.2',             |  | 
> |  |   T. Sjöstrand et al, Comput. Phys. Commun. 191 (2015) 159                   |  | 
> |  |   [arXiv:1410.3012 [hep-ph]]                                                 |  | 
> |  |                                                                              |  | 
> |  |   The main physics reference is the 'PYTHIA 6.4 Physics and Manual',         |  | 
> |  |   T. Sjöstrand, S. Mrenna and P. Skands, JHEP05 (2006) 026 [hep-ph/0603175]  |  | 
> |  |                                                                              |  | 
> |  |   PYTHIA is released under the GNU General Public Licence version 2 or later.|  | 
> |  |   Please respect the MCnet Guidelines for Event Generator Authors and Users. |  | 
> |  |                                                                              |  | 
> |  |   Disclaimer: this program comes without any guarantees.                     |  | 
> |  |   Beware of errors and use common sense when interpreting results.           |  | 
> |  |                                                                              |  | 
> |  |   Copyright (C) 2021 Torbjörn Sjöstrand                                      |  | 
> |  |                                                                              |  | 
> |  |                                                                              |  | 
> |  *------------------------------------------------------------------------------*  | 
> |                                                                                    | 
> *------------------------------------------------------------------------------------* 
>
> LHAPDF 6.4.0 loading /cvmfs/cms.cern.ch/el9_amd64_gcc11/external/lhapdf/6.4.0-72f55dc8a480105986848248c56e4054/share/LHAPDF/NNPDF31_nnlo_as_0118/NNPDF31_nnlo_as_0118_0000.dat
> NNPDF31_nnlo_as_0118 PDF set, member #0, version 1; LHAPDF ID = 303600
> LHAPDF 6.4.0 loading /cvmfs/cms.cern.ch/el9_amd64_gcc11/external/lhapdf/6.4.0-72f55dc8a480105986848248c56e4054/share/LHAPDF/NNPDF31_nnlo_as_0118/NNPDF31_nnlo_as_0118_0000.dat
> NNPDF31_nnlo_as_0118 PDF set, member #0, version 1; LHAPDF ID = 303600
> 
> *-------  PYTHIA Process Initialization  --------------------------*
> |                                                                  |
> | We collide p+ with p+ at a CM energy of 1.400e+04 GeV            |
> |                                                                  |
> |------------------------------------------------------------------|
> |                                                    |             |
> | Subprocess                                    Code |   Estimated |
> |                                                    |    max (mb) |
> |                                                    |             |
> |------------------------------------------------------------------|
> |                                                    |             |
> | non-diffractive                                101 |   5.536e+01 |
> | A B -> X B single diffractive                  103 |   6.442e+00 |
> | A B -> A X single diffractive                  104 |   6.442e+00 |
> | A B -> X X double diffractive                  105 |   8.881e+00 |
> | A B -> A X B central diffractive               106 |   1.289e+00 |
> |                                                                  |
> *-------  End PYTHIA Process Initialization -----------------------*
>
> *-------  PYTHIA Multiparton Interactions Initialization  ---------* 
> |                                                                  | 
> |                   sigmaNonDiffractive =    55.36 mb              | 
> |                                                                  | 
> |    pT0 =  1.44 gives sigmaInteraction =   485.21 mb: accepted    | 
> |                                                                  | 
> *-------  End PYTHIA Multiparton Interactions Initialization  -----* 
>
> *-------  PYTHIA Multiparton Interactions Initialization  ---------* 
> |                                                                  | 
> |                          diffraction XB                          | 
> |                                                                  | 
> |   diffractive mass = 1.00e+01 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.13 gives sigmaInteraction =     1.43 mb: rejected    | 
> |    pT0 =  1.02 gives sigmaInteraction =     2.29 mb: rejected    | 
> |    pT0 =  0.92 gives sigmaInteraction =     3.68 mb: rejected    | 
> |    pT0 =  0.83 gives sigmaInteraction =     6.02 mb: rejected    | 
> |    pT0 =  0.74 gives sigmaInteraction =    10.15 mb: rejected    | 
> |    pT0 =  0.67 gives sigmaInteraction =    17.75 mb: accepted    | 
> |   diffractive mass = 1.62e+01 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.15 gives sigmaInteraction =     2.31 mb: rejected    | 
> |    pT0 =  1.04 gives sigmaInteraction =     3.58 mb: rejected    | 
> |    pT0 =  0.93 gives sigmaInteraction =     5.67 mb: rejected    | 
> |    pT0 =  0.84 gives sigmaInteraction =     9.03 mb: rejected    | 
> |    pT0 =  0.76 gives sigmaInteraction =    14.76 mb: accepted    | 
> |   diffractive mass = 2.63e+01 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.17 gives sigmaInteraction =     3.37 mb: rejected    | 
> |    pT0 =  1.05 gives sigmaInteraction =     5.10 mb: rejected    | 
> |    pT0 =  0.95 gives sigmaInteraction =     7.82 mb: rejected    | 
> |    pT0 =  0.85 gives sigmaInteraction =    12.27 mb: accepted    | 
> |   diffractive mass = 4.26e+01 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.19 gives sigmaInteraction =     4.57 mb: rejected    | 
> |    pT0 =  1.07 gives sigmaInteraction =     6.78 mb: rejected    | 
> |    pT0 =  0.96 gives sigmaInteraction =    10.18 mb: rejected    | 
> |    pT0 =  0.87 gives sigmaInteraction =    15.52 mb: accepted    | 
> |   diffractive mass = 6.90e+01 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.21 gives sigmaInteraction =     5.86 mb: rejected    | 
> |    pT0 =  1.09 gives sigmaInteraction =     8.44 mb: rejected    | 
> |    pT0 =  0.98 gives sigmaInteraction =    12.58 mb: accepted    | 
> |   diffractive mass = 1.12e+02 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.23 gives sigmaInteraction =     7.36 mb: rejected    | 
> |    pT0 =  1.11 gives sigmaInteraction =    10.46 mb: rejected    | 
> |    pT0 =  0.99 gives sigmaInteraction =    15.01 mb: accepted    | 
> |   diffractive mass = 1.81e+02 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.25 gives sigmaInteraction =     9.14 mb: rejected    | 
> |    pT0 =  1.12 gives sigmaInteraction =    12.59 mb: accepted    | 
> |   diffractive mass = 2.94e+02 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.27 gives sigmaInteraction =    11.32 mb: accepted    | 
> |   diffractive mass = 4.76e+02 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.29 gives sigmaInteraction =    14.31 mb: accepted    | 
> |   diffractive mass = 7.72e+02 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.31 gives sigmaInteraction =    18.18 mb: accepted    | 
> |   diffractive mass = 1.25e+03 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.33 gives sigmaInteraction =    23.21 mb: accepted    | 
> |   diffractive mass = 2.03e+03 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.35 gives sigmaInteraction =    29.53 mb: accepted    | 
> |   diffractive mass = 3.29e+03 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.37 gives sigmaInteraction =    37.61 mb: accepted    | 
> |   diffractive mass = 5.33e+03 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.40 gives sigmaInteraction =    47.66 mb: accepted    | 
> |   diffractive mass = 8.64e+03 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.42 gives sigmaInteraction =    59.14 mb: accepted    | 
> |   diffractive mass = 1.40e+04 GeV and sigmaNorm =    10.00 mb    | 
> |    pT0 =  1.44 gives sigmaInteraction =    74.34 mb: accepted    | 
> |                                                                  | 
> *-------  End PYTHIA Multiparton Interactions Initialization  -----* 
> PYTHIA Warning in MultipartonInteractions::init: maximum increased by factor 4.865
> 
> ........
> 
> *-------  PYTHIA Flag + Mode + Parm + Word + FVec + MVec + PVec + WVec Settings (changes only)  ------------------* 
> |                                                                                                                 | 
> | Name                                          |                      Now |      Default         Min         Max | 
> |                                               |                          |                                      | 
> | Check:epTolErr                                |                0.0100000 |   1.0000e-04                         | 
> | ColourReconnection:range                      |                  5.17600 |      1.80000         0.0    10.00000 | 
> | Main:timesAllowErrors                         |                    10000 |           10           0             | 
> | MultipartonInteractions:alphaSorder           |                        2 |            1           0           2 | 
> | MultipartonInteractions:alphaSvalue           |                  0.11800 |      0.13000   0.0600000     0.25000 | 
> | MultipartonInteractions:bProfile              |                        2 |            3           0           4 | 
> | MultipartonInteractions:coreFraction          |                  0.63000 |      0.50000         0.0     1.00000 | 
> | MultipartonInteractions:coreRadius            |                  0.76340 |      0.40000     0.10000     1.00000 | 
> | MultipartonInteractions:ecmPow                |                0.0334400 |      0.21500         0.0     0.50000 | 
> | MultipartonInteractions:pT0Ref                |                  1.41000 |      2.28000     0.50000    10.00000 | 
> | Next:numberShowEvent                          |                        0 |            1           0             | 
> | ParticleDecays:allowPhotonRadiation           |                       on |          off                         | 
> | ParticleDecays:limitTau0                      |                       on |          off                         | 
> | PDF:pSet                                      | LHAPDF6:NNPDF31_nnlo_as_0118 |           13                     | 
> | SigmaProcess:alphaSorder                      |                        2 |            1           0           2 | 
> | SigmaProcess:alphaSvalue                      |                  0.11800 |      0.13000   0.0600000     0.25000 | 
> | SigmaTotal:mode                               |                        0 |            1           0           4 | 
> | SigmaTotal:sigmaEl                            |                 21.89000 |     25.00000         0.0             | 
> | SigmaTotal:sigmaTot                           |                100.30900 |    100.00000         0.0             | 
> | SigmaTotal:zeroAXB                            |                      off |           on                         | 
> | SLHA:minMassSM                                |                 1000.000 |    100.00000                         | 
> | SoftQCD:inelastic                             |                       on |          off                         | 
> | SpaceShower:alphaSorder                       |                        2 |            1           0           2 | 
> | SpaceShower:alphaSvalue                       |                  0.11800 |      0.13650   0.0600000     0.25000 | 
> | TimeShower:alphaSorder                        |                        2 |            1           0           2 | 
> | TimeShower:alphaSvalue                        |                  0.11800 |      0.13650   0.0600000     0.25000 | 
> | Tune:preferLHAPDF                             |                        2 |            1           0           2 | 
> |                                                                                                                 | 
> *-------  End PYTHIA Flag + Mode + Parm + Word + FVec + MVec + PVec + WVec Settings  -----------------------------* 
> 
> --------  PYTHIA Particle Data Table (changed only)  ------------------------------------------------------------------------------
> 
>      id   name            antiName         spn chg col      m0        mWidth      mMin       mMax       tau0    res dec ext vis wid
>             no onMode   bRatio   meMode     products 
> 
> no particle data has been changed from its default value 
> 
> --------  End PYTHIA Particle Data Table  -----------------------------------------------------------------------------------------
> 
> 
> *-------  PYTHIA Flag + Mode + Parm + Word + FVec + MVec + PVec + WVec Settings (changes only)  ------------------* 
> |                                                                                                                 | 
> | Name                                          |                      Now |      Default         Min         Max | 
> |                                               |                          |                                      | 
> | Next:numberShowEvent                          |                        0 |            1           0             | 
> | ParticleDecays:allowPhotonRadiation           |                       on |          off                         | 
> | ParticleDecays:limitTau0                      |                       on |          off                         | 
> | ProcessLevel:all                              |                      off |           on                         | 
> |                                                                                                                 | 
> *-------  End PYTHIA Flag + Mode + Parm + Word + FVec + MVec + PVec + WVec Settings  -----------------------------* 
> 
> --------  PYTHIA Particle Data Table (changed only)  ------------------------------------------------------------------------------
> 
>     id   name            antiName         spn chg col      m0        mWidth      mMin       mMax       tau0    res dec ext vis wid
>             no onMode   bRatio   meMode     products 
> 
> no particle data has been changed from its default value 
>
> --------  End PYTHIA Particle Data Table  -----------------------------------------------------------------------------------------
>
> Begin processing the 1st record. Run 1, Event 1, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:13.256 CEST
> 
> --------  PYTHIA Info Listing  ---------------------------------------- 
>  
> Beam A: id =   2212, pz =  7.000e+03, e =  7.000e+03, m =  9.383e-01.
> Beam B: id =   2212, pz = -7.000e+03, e =  7.000e+03, m =  9.383e-01.
> 
> In 1: id =   21, x =  5.261e-02, pdf =  2.316e+00 at Q2 =  5.847e+00.
> In 2: id =   21, x =  6.884e-06, pdf =  1.134e+01 at same Q2.
> 
> Process non-diffractive with code 101 is 2 -> 2.
> Subprocess g g -> g g with code 111 is 2 -> 2.
> It has sHat =  7.098e+01,    tHat = -6.455e+01,    uHat = -6.430e+00,
>       pTHat =  2.418e+00,   m3Hat =  0.000e+00,   m4Hat =  0.000e+00,
>    thetaHat =  2.530e+00,  phiHat =  5.367e-02.
>     alphaEM =  7.524e-03,  alphaS =  2.599e-01    at Q2 =  7.930e+00.
> 
> Impact parameter b =  1.866e+00 gives enhancement factor =  4.980e-03.
> Max pT scale for MPI =  2.418e+00, ISR =  2.418e+00, FSR =  2.418e+00.
> Number of MPI =     1, ISR =     0, FSRproc =     3, FSRreson =     0.
> 
> --------  End PYTHIA Info Listing  ------------------------------------
> 
> --------  PYTHIA Event Listing  (hard process)  -----------------------------------------------------------------------------------
> 
>   no         id  name            status     mothers   daughters     colours      p_x        p_y        p_z         e          m 
>    0         90  (system)           -11     0     0     0     0     0     0      0.000      0.000      0.000  14000.000  14000.000
>    1       2212  (p+)               -12     0     0     3     0     0     0      0.000      0.000   7000.000   7000.000      0.938
>    2       2212  (p+)               -12     0     0     4     0     0     0      0.000      0.000  -7000.000   7000.000      0.938
>    3         21  (g)                -21     1     0     5     6   101   102      0.000      0.000    368.240    368.240      0.000
>    4         21  (g)                -21     2     0     5     6   103   101      0.000      0.000     -0.048      0.048      0.000
>    5         21  g                   23     3     4     0     0   103   104      0.130      2.415     33.312     33.400      0.000
>    6         21  g                   23     3     4     0     0   104   102     -0.130     -2.415    334.880    334.888      0.000
>                                  Charge sum:  0.000           Momentum sum:      0.000      0.000    368.192    368.288      8.425
> --------  End PYTHIA Event Listing  -----------------------------------------------------------------------------------------------
> #--------------------------------------------------------------------------
> #                         FastJet release 3.4.0
> #                 M. Cacciari, G.P. Salam and G. Soyez                  
> #     A software package for jet finding and analysis at colliders      
> #                           http://fastjet.fr                           
> #	                                                                      
> # Please cite EPJC72(2012)1896 [arXiv:1111.6097] if you use this package
> # for scientific work and optionally PLB641(2006)57 [hep-ph/0512210].   
> #                                                                       
> # FastJet is provided without warranty under the GNU GPL v2 or higher.  
> # It uses T. Chan's closest pair algorithm, S. Fortune's Voronoi code
> # and 3rd party plugin jet algorithms. See COPYING file for details.
> #--------------------------------------------------------------------------
> Begin processing the 2nd record. Run 1, Event 2, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:14.697 CEST
> Begin processing the 3rd record. Run 1, Event 3, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:14.719 CEST
> Begin processing the 4th record. Run 1, Event 4, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:15.498 CEST
> Begin processing the 5th record. Run 1, Event 5, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:16.285 CEST
> Begin processing the 6th record. Run 1, Event 6, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:17.026 CEST
> Begin processing the 7th record. Run 1, Event 7, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:18.263 CEST
> Begin processing the 8th record. Run 1, Event 8, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:18.925 CEST
> Begin processing the 9th record. Run 1, Event 9, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:19.047 CEST
>  PYTHIA Warning in SimpleSpaceShower::pT2nextQCD: weight above unity  
> Begin processing the 10th record. Run 1, Event 10, LumiSection 1 on stream 0 at 29-Apr-2024 12:01:20.764 CEST
> 
> *-------  PYTHIA Event and Cross Section Statistics  -------------------------------------------------------------*
> |                                                                                                                 |
> | Subprocess                                    Code |            Number of events       |      sigma +- delta    |
> |                                                    |       Tried   Selected   Accepted |     (estimated) (mb)   |
> |                                                    |                                   |                        |
> |-----------------------------------------------------------------------------------------------------------------|
> |                                                    |                                   |                        |
> | non-diffractive                                101 |           7          7          7 |   5.536e+01  0.000e+00 |
> | A B -> X B single diffractive                  103 |           1          1          1 |   6.442e+00  6.442e+00 |
> | A B -> A X single diffractive                  104 |           0          0          0 |   0.000e+00  0.000e+00 |
> | A B -> X X double diffractive                  105 |           1          1          1 |   8.881e+00  8.881e+00 |
> | A B -> A X B central diffractive               106 |           1          1          1 |   1.289e+00  1.289e+00 |
> |                                                    |                                   |                        |
> | sum                                                |          10         10         10 |   7.198e+01  1.105e+01 |
> |                                                                                                                 |
> *-------  End PYTHIA Event and Cross Section Statistics ----------------------------------------------------------*
>
> *-------  PYTHIA Error and Warning Messages Statistics  ----------------------------------------------------------* 
> |                                                                                                                 | 
> |  times   message                                                                                                | 
> |                                                                                                                 | 
> |      3   Warning in MultipartonInteractions::init: maximum increased                                            | 
> |      1   Warning in SimpleSpaceShower::pT2nextQCD: weight above unity                                           | 
> |                                                                                                                 | 
> *-------  End PYTHIA Error and Warning Messages Statistics  ------------------------------------------------------* 
>
> *-------  PYTHIA Event and Cross Section Statistics  -------------------------------------------------------------*
> |                                                                                                                 |
> | Subprocess                                    Code |            Number of events       |      sigma +- delta    |
> |                                                    |       Tried   Selected   Accepted |     (estimated) (mb)   |
> |                                                    |                                   |                        |
> |-----------------------------------------------------------------------------------------------------------------|
> |                                                    |                                   |                        |
> | non-diffractive                                101 |           7          7          7 |   5.536e+01  0.000e+00 |
> | A B -> X B single diffractive                  103 |           1          1          1 |   6.442e+00  6.442e+00 |
> | A B -> A X single diffractive                  104 |           0          0          0 |   0.000e+00  0.000e+00 |
> | A B -> X X double diffractive                  105 |           1          1          1 |   8.881e+00  8.881e+00 |
> | A B -> A X B central diffractive               106 |           1          1          1 |   1.289e+00  1.289e+00 |
> |                                                    |                                   |                        |
> | sum                                                |          10         10         10 |   7.198e+01  1.105e+01 |
> |                                                                                                                 |
> *-------  End PYTHIA Event and Cross Section Statistics ----------------------------------------------------------*
>
> *-------  PYTHIA Error and Warning Messages Statistics  ----------------------------------------------------------* 
> |                                                                                                                 | 
> |  times   message                                                                                                | 
> |                                                                                                                 | 
> |      3   Warning in MultipartonInteractions::init: maximum increased                                            | 
> |      1   Warning in SimpleSpaceShower::pT2nextQCD: weight above unity                                           | 
> |                                                                                                                 | 
> *-------  End PYTHIA Error and Warning Messages Statistics  ------------------------------------------------------*
>
> ------------------------------------
> GenXsecAnalyzer:
> ------------------------------------
> Before Filter: total cross section = 7.198e+10 +- 1.105e+10 pb
> Filter efficiency (taking into account weights)= (10) / (10) = 1.000e+00 +- 0.000e+00
> Filter efficiency (event-level)= (10) / (10) = 1.000e+00 +- 0.000e+00    [TO BE USED IN MCM]
> 
> After filter: final cross section = 7.198e+10 +- 1.105e+10 pb
> After filter: final fraction of events with negative weights = 0.000e+00 +- 0.000e+00
> After filter: final equivalent lumi for 1M events (1/fb) = 1.389e-08 +- 2.132e-09
> Thanks for using LHAPDF 6.4.0. Please make sure to cite the paper:
>   Eur.Phys.J. C75 (2015) 3, 132  (http://arxiv.org/abs/1412.7420)
> ============================================= 
> ```
{: .solution}


> ## Question 12.1
> What is the file size of `step1.root`? <br>
> For CMSDAS@CERN{{ site.year }} please submit your answers for the [CMSDAS@CERN{{ site.year }} Google Form third set][Set3_form].
{: .challenge}

## Generate MC dataset using CRAB

CRAB is handled by a *configuration file*. In CRAB3, the configuration file is in Python language. Here we give an example CRAB configuration file to run the `CMSDAS_MC_generation.py` MC event generation code. You can download a copy of [crabConfig_MC_generation.py]({{ page.root }}{% link code/crabConfig_MC_generation.py %}).

> ## Hint
> You can easily download the needed files by running the following commands:
> ~~~shell
> wget https://gitlab.cern.ch/cmsdas-cern-2024/cms-das-pre-exercises/-/raw/master/code/crabConfig_MC_generation.py
> ~~~
> {: . source}
{: .callout}

Below you also find the file:
> ## Show/Hide
>
> ```
> from WMCore.Configuration import Configuration
> config = Configuration()
>
> config.section_("General")
> config.General.requestName = 'CMSDAS_MC_generation_test0'
> config.General.workArea = 'crab_projects'
>
> config.section_("JobType")
> config.JobType.pluginName = 'PrivateMC'
> config.JobType.psetName = 'CMSDAS_MC_generation.py'
>
> config.section_("Data")
> config.Data.outputPrimaryDataset = 'MinBias'
> config.Data.splitting = 'EventBased'
> config.Data.unitsPerJob = 10
> NJOBS = 10  # This is not a configuration parameter, but an auxiliary variable that we use in the next line.
> config.Data.totalUnits = config.Data.unitsPerJob * NJOBS
> config.Data.publication = True
> config.Data.outputDatasetTag = 'CMSDAS2024_CRAB3_MC_generation_test0'
>
> config.section_("Site")
> config.Site.storageSite = 'T3_CH_CERNBOX'
> ```
{: .solution}
Put the copy of  `crabConfig_MC_generation.py` under `YOURWORKINGAREA/CMSSW_13_0_17/src`.


All available CRAB configuration parameters are defined at [CRAB3ConfigurationFile](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile).

Now let us try to submit this job via crab by

```shell
crab submit -c crabConfig_MC_generation.py
```
{: .source}

For the detail of the crab command, you can find them from [CRABCommands](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookCRAB3Tutorial#CRAB_commands). You will be requested to enter your grid certificate password. <br>
Then you should get an output similar to this:
```
Will use CRAB configuration file crabConfig_MC_generation.py
Importing CMSSW configuration CMSDAS_MC_generation.py
Finished importing CMSSW configuration CMSDAS_MC_generation.py
Sending the request to the server at cmsweb.cern.ch
Success: Your task has been delivered to the prod CRAB3 server.
Task name: 240429_102627:<username>_crab_CMSDAS_MC_generation_test0
Project dir: crab_projects/crab_CMSDAS_MC_generation_test0
Please use ' crab status -d crab_projects/crab_CMSDAS_MC_generation_test0 ' to check how the submission process proceeds.
Log file is /afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_MC_generation_test0/crab.log
```
{: .output}

 Now you might notice a directory called `crab_projects` is created under `CMSSW_13_0_17/src/`. *See what is under that directory.* After you submitted the job successfully (give it a few moments), you can check the status of a task by executing the following CRAB command:

 ```shell
 crab status [-d] <CRAB-project-directory>
 ```
 {: .source}
 In our case, we run:

 ```shell
crab status crab_projects/crab_CMSDAS_MC_generation_test0
```
{: .source}

The `crab status` command will produce an output containing the task name, the status of the task as a whole, the details of how many jobs are in which state (submitted, running, transfering, finished, cooloff, etc.) and the location of the CRAB log (`crab.log`) file. It will also print the URLs of two web pages that one can use to monitor the jobs. In summary, it should look something like this:

```
Rucio client intialized for account <username>
CRAB project directory:		/afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_MC_generation_test0
Task name:			<date>_<time>:<username>_crab_CMSDAS_MC_generation_test0
Grid scheduler - Task Worker:	crab3@vocms0120.cern.ch - crab-prod-tw02
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/<date>_<time>%3A<username>_crab_CMSDAS_MC_generation_test0
Dashboard monitoring URL:	https://monit-grafana.cern.ch/d/cmsTMDetail/cms-task-monitoring-task-view?orgId=11&var-user=<username>&var-task=<date>_<time>%3A<username>_crab_CMSDAS_MC_generation_test0&from=1714382956000&to=now
Task bootstrapped at <date> <time> UTC. 50 seconds ago
Status information will be available within a few minutes
Log file is /afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_MC_generation_test0/crab.log
```
{: .output}

 Now you can take a break and have some fun. Come back after couple hours or so and check the status again.

```
Rucio client intialized for account <username>
CRAB project directory:		/afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_MC_generation_test0
Task name:			<date>_<time>:<username>_crab_CMSDAS_MC_generation_test0
Grid scheduler - Task Worker:	crab3@vocms0120.cern.ch - crab-prod-tw02
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/<date>_<time>%3A<username>_crab_CMSDAS_MC_generation_test0
Dashboard monitoring URL:	https://monit-grafana.cern.ch/d/cmsTMDetail/cms-task-monitoring-task-view?orgId=11&var-user=<username>&var-task=<date>_<time>%3A<username>_crab_CMSDAS_MC_generation_test0&from=1714382956000&to=now
Status on the scheduler:	COMPLETED

Jobs status:                    finished     		100.0% (10/10)

Publication status of 1 dataset(s):	done         		100.0% (10/10)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/MinBias/<username>-CMSDAS2024_CRAB3_MC_generation_test0-<hash>/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FMinBias%2F<username>-CMSDAS2024_CRAB3_MC_generation_test0-<hash>%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value for failed jobs (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 218MB min, 1320MB max, 910MB ave
 * Runtime: 0:04:31 min, 0:11:05 max, 0:07:14 ave
 * CPU eff: 32% min, 78% max, 51% ave
 * Waste: 0:50:49 (41% of total)

Log file is /afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_MC_generation_test0/crab.log
```
{: .output}

**Note**: If at `lxplus`, it will write out to your eos area. You can access them from `/eos/user/$U/$USER/SUBDIR` with `SUBDIR` being the subdirectory name you provided. Take a look at that directory. (In our example we looked at `MinBias` and named the task `CMSDAS2021_CRAB3_MC_generation_test0`. The subsequent date string depends when you started your task.)

From the bottom of the output, you can see the name of the dataset and the DAS link to it. Congratulations! This is the your first CMS dataset.

> ## Question 12.2
> What is the name of the dataset you produced? <br>
> For CMSDAS@CERN{{ site.year }} please submit your answers for the [CMSDAS@CERN{{ site.year }} Google Form third set][Set3_form].
{: .challenge}

# Exercise 13 - Running on a dataset with CRAB

Now we're going to apply what you've learned using CRAB to the `MiniAOD` exercises you've been working on in the first two sets of exercises. Make sure that you finished and still have the scripts from [Exercise 7]({{ page.root }}{% link _episodes/02-CMSDataAnalysisSchoolPreExerciseSecondSet.md %}#exercise-7---slim-miniaod-sample-to-reduce-its-size-by-keeping-only-muon-and-electron-branches) under the `YOURWORKINGAREA/CMSSW_13_0_17/src`.

##  Set up CRAB to run your MiniAOD jobs

If you forget, go back to the `YOURWORKINGAREA/CMSSW_13_0_17/src` and setup crab.

```shell
cmsenv
```
{: .source}

We will make another CRAB config file: `crabConfig_data_slimMiniAOD.py`. Copy it from here: [crabConfig_data_slimMiniAOD.py]({{ page.root }}{% link code/crabConfig_data_slimMiniAOD.py %}) and find it below:

> ## Hint
> You can easily download the needed files by running the following commands:
> ~~~shell
> wget https://gitlab.cern.ch/cmsdas-cern-2024/cms-das-pre-exercises/-/raw/master/code/crabConfig_data_slimMiniAOD.py
> ~~~
> {: . source}
{: .callout}

> ## Show/Hide
>
> ```
> from WMCore.Configuration import Configuration
> config = Configuration()
>
> config.section_("General")
> config.General.requestName = 'CMSDAS_Data_analysis_test0'
> config.General.workArea = 'crab_projects'
>
> config.section_("JobType")
> config.JobType.pluginName = 'Analysis'
> config.JobType.psetName = 'slimMiniAOD_data_MuEle_cfg.py'
> config.JobType.allowUndistributedCMSSW = True
>
> config.section_("Data")
> config.Data.inputDataset = '/Muon0/Run2023C-22Sep2023_v4-v1/MINIAOD'
> config.Data.inputDBS = 'global'
> config.Data.splitting = 'LumiBased'
> config.Data.unitsPerJob = 50
> config.Data.lumiMask = 'https://cms-service-dqmdc.web.cern.ch/CAF/certification/Collisions23/Cert_Collisions2023_eraC_367095_368823_Golden.json'
> config.Data.runRange = '368609'
>
> config.section_("Site")
> config.Site.storageSite = 'T3_CH_CERNBOX'
> ```
{: .solution}

 Most of this file should be familiar by now, but a few things may be new. The `runRange` parameter is used to further limit your jobs to a specific run or a range of what is in the `lumiMask` file. This is needed if your two input datasets overlap. That way you can control which events come from which datasets. Instructions how to do this are at [https://twiki.cern.ch/twiki/bin/viewauth/CMS/PdmVAnalysisSummaryTable](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PdmVAnalysisSummaryTable). You can find the year specific instructions by clicking any of the links at the bottom.

## Run CRAB

 Now go through the same process for this config file. You submit it with

 ```shell
 crab submit -c crabConfig_data_slimMiniAOD.py
 ```
 {: .source}

 and check the status with

 ```shell
 crab status
 ```
 {: .source}

 After a while, you should see something like below:

```
Rucio client intialized for account <username>
CRAB project directory:		/afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_Data_analysis_test0
Task name:			<date>_<time>:<username>_crab_CMSDAS_Data_analysis_test0
Grid scheduler - Task Worker:	crab3@vocms0119.cern.ch - crab-prod-tw02
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/<date>_<time>%3A<username>_crab_CMSDAS_Data_analysis_test0
Dashboard monitoring URL:	https://monit-grafana.cern.ch/d/cmsTMDetail/cms-task-monitoring-task-view?orgId=11&var-user=<username>&var-task=<date>_<time>%3A<username>_crab_CMSDAS_Data_analysis_test0&from=1714389824000&to=now
Status on the scheduler:	SUBMITTED

Jobs status:                    finished     		100.0% (24/24)

Publication status of 1 dataset(s):	done         		100.0% (24/24)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/Muon0/<username>-crab_CMSDAS_Data_analysis_test0-<hash>/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FMuon0%2F<username>-crab_CMSDAS_Data_analysis_test0-<hash>%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value for failed jobs (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 377MB min, 1158MB max, 651MB ave
 * Runtime: 0:02:00 min, 0:25:12 max, 0:11:43 ave
 * CPU eff: 20% min, 76% max, 62% ave
 * Waste: 0:52:30 (16% of total)

Log file is /afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_Data_analysis_test0/crab.log
```
{: .output}

## Create reports of data analyzed

Once all jobs are **finished** (see `crab status` above) you can report:

```shell
crab report
````
{: .source}

You'll get something like this
```
Running crab status first to fetch necessary information.
Rucio client intialized for account anstahll
Will save lumi files into output directory /afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_Data_analysis_test0/results
Summary from jobs in status 'finished':
  Number of files processed: 121
  Number of events read: X
  Number of events written in EDM files: X
  Number of events written in TFileService files: 0
  Number of events written in other type of files: 0
  Processed lumis written to processedLumis.json
Summary from output datasets in DBS:
  Number of events:
    /Muon0/<username>-crab_CMSDAS_Data_analysis_test0-<hash>/USER: 68340001
  Output datasets lumis written to outputDatasetsLumis.json
Additional report lumi files:
  Input dataset lumis (from DBS, at task submission time) written to inputDatasetLumis.json
  Lumis to process written to lumisToProcess.json
Log file is /afs/cern.ch/work/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_Data_analysis_test0/crab.log
```
{: .output}

 `crab report` prints to the screen how many events were analyzed.


> ## Question 13
> How many events were analyzed? (n.b. the number in the above example were replaced with `X`) <BR>
> For CMSDAS@CERN{{ site.year }} please submit your answers for the [CMSDAS@CERN{{ site.year }} Google Form third set][Set3_form].
{: .challenge}

## Optional: View the reconstructed Z peak in the combined data

> ## Note
> You will be doing a short analysis later when going to [exercise set number four]({{ page.root }}{% link _episodes/04-CMSDataAnalysisSchoolPreExerciseFourthSet.md %}).
>
{: .callout}

Use the `FWLiteHistograms` executable you were using in the previous exercises to aggregate the data from all the CRAB output files. The root files created in the above step have been kept at the directory below: `/eos/user/$U/$USER/Muon0/crab_CMSDAS_Data_analysis_test0/` One can use the command:

``` shell
FWLiteHistograms inputFiles=File1,File2,File3,... outputFile=ZPeak_data.root maxEvents=-1 outputEvery=100
```
{: .source}

In my case, `File1=/eos/user/<initial>/<username>/Muon0/crab_CMSDAS_Data_analysis_test0/<date>_<time>/0000/slimMiniAOD_data_MuEle_1.root` etc.. Make sure there is no space in `File1,File2,File3,...`

You may look at `ZPeak_data.root` using `TBrowser`.

# Exercise 14  - Combining the data and calculating luminosity

> ## Note
> This last exercise in this set is done on **lxplus**.
>
{: .callout}

## Install the BRIL Work Suite

We will use the BRIL work suite, a commandline toolkit for CMS Beam Radiation Instrumentation and Luminosity to calculate the total luminosity of the data we ran over.

Refer to [the documentation](https://cmslumi.web.cern.ch/) for further information on BRIL.

Since BRIL workspace is not yet fully compatible with AlmaLinux9, we will use singularity to work with AlmaLinux8 instead, by doing:
```shell
cmssw-el8
```
{: .source}

> ## Note
> Singularity is a container platform. It allows to create and run containers that package up pieces of software in a way that is portable and reproducible. The CMS computing framework allows to run singularity in lxplus by simply calling `cmssw-xx` (for instance `cmssw-el8` for AlmaLinux8), which makes use of unpacked images available under `/cvmfs/unpacked.cern.ch/registry.hub.docker.com/`. For more information of CMS singularity options please check the [CMS website](https://cms-sw.github.io/singularity.html).
> Singularity will be covered in the [seventh set of exercises]({{ page.root }}{% link _episodes/07-CMSDataAnalysisSchoolPreExerciseSeventhSet.md %}).
{: .callout}

Enter the following command to install the BRIL workspace (brilws):

``` shell
/cvmfs/cms-bril.cern.ch/brilconda3/bin/python3 -m pip install --user --upgrade brilws
```
{: .source}

Then set up the BRIL environment:

``` shell
export PATH=$HOME/.local/bin:/cvmfs/cms-bril.cern.ch/brilconda3/bin:$PATH
```
{: .source}

When running `crab report`, the report will give you the location of a **JSON-formatted file** containing the luminosity information
```
Will save lumi files into output directory /afs/cern.ch/user/<initial>/<username>/CMSDAS2024/Pre-exercises/CMSSW_13_0_17/src/crab_projects/crab_CMSDAS_Data_analysis_test0/results
```
{: .output}

This directory contains various luminosity files. Let's figure out how much luminosity was run on by our jobs.

First step is to copy the `processedLumis.json` file to your `~/.local/bin/` folder:
``` shell
cp [lumi directory]/processedLumis.json ~/.local/bin/
```
{: .source}

Here, `[lumi directory]` is the directory reported by `crab report`.

## Find the luminosity for the dataset

We now let `brilcalc` calculate the luminosity we processed with our jobs using the json file by typing following commands:
``` shell
cd ~/.local/bin/
brilcalc lumi --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json -u /fb -i processedLumis.json
```

> ## Note
> The normtag contains the latest official calibrations for the appropriate run period. Please find the list of recommended normtags and the associated uncertainties in the [LUMI POG twiki page](https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM#CurRec). There are two main normtags that you should be aware of:
>    * The physics normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_PHYSICS.json covers all of Run 2 and 2022 for which there is a final approved number for physics. In general you should use this normtag whenever available.
>    * The preliminary normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json contains the best preliminary calibrations and can be used for cases when an approved number is not available yet (i.e. for 2023).
{: .callout}

 The end of the output should look similar to this (note this example summary is for a different json file):
 ```
 #Summary:
 +-------+------+-------+-------+-------------------+------------------+
 | nfill | nrun | nls   | ncms  | totdelivered(/fb) | totrecorded(/fb) |
 +-------+------+-------+-------+-------------------+------------------+
 | 9     | 37   | 17377 | 17377 | 2.761             | 2.646            |
 +-------+------+-------+-------+-------------------+------------------+
 #Check JSON:
 #(run,ls) in json but not in results: [(275890, 721)]
 ```
 {: .output}

In the example of that other json file, the total recorded luminosity for those CRAB jobs is 2.6 fb<sup>-1</sup>.

Now we can exit the singularity by doing `exit`.

> ## Question 14
>  What is the reported number of inverse femtobarns analyzed? (n.b. it is not the same sample as listed above with luminosity 2.6<sup>-1</sup>)
> For CMSDAS@CERN{{ site.year }} please submit your answers for the [CMSDAS@CERN{{ site.year }} Google Form third set][Set3_form].
{: .challenge}


#  Where to find more on CRAB

- [CRAB Home](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab) <br>
- [CRAB FAQ](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3FAQ) <br>
- [CRAB troubleshooting guide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3Troubleshoot): Steps to address the problems you experience with CRAB and how to ask for support. <br>
- [CMS Computing Tools mailing list](mailto:cmstalk+computing-tools@dovecotmta.cern.ch), where to send feedback and ask support in case of jobs problem (please send to us your crab task HELP URL from crab status output). <br>

Important CRAB announcements will be announced on the [CERN Computing CMS talk forum](https://cms-talk.web.cern.ch/tags/c/offcomp/11/announcements). 

{% include links.md %}

[Set3_form]: https://forms.gle/6jmJgiVrerbQc2f47